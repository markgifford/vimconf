" Vundle setup
set nocompatible    " Of course
filetype off        " also required

" set the runtime path to include Vundle and initialize
set runtimepath+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" PLUGINS

" Git integration
Plugin 'tpope/vim-fugitive'

" NERDTree file browser
Plugin 'scrooloose/nerdtree'

" Fancy status bar
Plugin 'Lokaltog/vim-powerline'

" Settings for solarized-dark colour scheme plugin
Plugin 'altercation/vim-colors-solarized'

" Better tab completion
Plugin 'ervandew/supertab'

" Discourage bad habits! E.g. using j,j,j,j,j to navigate
Plugin 'takac/vim-hardtime'

" END OF PLUGINS

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" To ignore plugin indent changes, instead use:
" filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" map leader from default \ to , instead
let mapleader=","

" Set the timeout after which leader key becomes inactive to 1.2 secs. I'm slow!
set timeoutlen=1200

"Get visual feedback in bottom when leader key is pressed
set showcmd

" Sensible encoding
" Also necessary to show Unicode glyphs in vim-powerline
set encoding=utf-8

" Auto/smart indent
set autoindent smartindent

" Get rid of unsaved buffer message when closing without saving
set hidden

" Always have at least 5 lines visible above and below the cursor
set scrolloff=5
" This can be set to a huge number like 999 to always ensure line below cursor
" is always vertically centered. zz is provided by default to manually center
" the cursor line

" Set tabs to be two spaces wide
set softtabstop=2
set shiftwidth=2
set tabstop=2
set expandtab

" Enable filetypes
filetype on " OK to enable this now Vundle's done its stuff
filetype plugin on
filetype indent on
syntax on

" Set to always show the tabline even if not using multiple files in tabs
"set showtabline=2

" Better searching
set ignorecase smartcase incsearch
set hlsearch showmatch
" Use Ctrl-l to turn off the highlighting. Chould probably remap to <leader-l>
nnoremap <silent> <C-l> :nohl<CR><C-l>

" Nice line numbering. relativenumber + number = kind of hybrid mode. Only on 7.4+
set relativenumber number
nmap <leader>srn :set relativenumber!
nmap <leader>sn :set number!<CR>
" Use ,sn and ,srn to toggle them

" Testing automatic PHP folding
let php_folding = 1        "Set PHP folding of classes and functions.
let php_htmlInStrings = 1  "Syntax highlight HTML code inside PHP strings.
let php_sql_query = 1      "Syntax highlight SQL code inside PHP strings.
let php_noShortTags = 1    "Disable PHP short tags.

" Needed so I can edit crontab on Mac apparently
set backupskip=/tmp/*,/private/tmp/*

" NERDTree stuff

" Open a nerdtree automatically when vim opens up
"autocmd vimenter * NERDTree
" Close vim if nerdtree is the only window left open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
" open and close NERDTree with leader n
map <leader>n :NERDTreeToggle<CR> 

" vim-powerline stuff

let g:Powerline_symbols = 'fancy'
" Always show the statusline
set laststatus=2
" Explicitly tell Vim that the terminal supports 256 colors
set t_Co=256
let g:Powerline_colorscheme = 'solarized256'
syntax enable
colorscheme solarized
set background=dark

" Navigation

nnoremap <Up> <nop>
nnoremap <Down> <nop>
nnoremap <Right> <nop>
nnoremap <Left> <nop>
inoremap <Up> <nop>
inoremap <Down> <nop>
inoremap <Right> <nop>
inoremap <Left> <nop>
" Make up/down j/k go up and down by file line instead of screen line
nnoremap j gj
nnoremap k gk

"Hit jj together when in insert mode to exit back to normal mode
inoremap jj <ESC>

" Better <TAB> navigation

nnoremap th :tabfirst<CR>
nnoremap tj :tabnext<CR>
nnoremap tk :tabprev<CR>
nnoremap tl :tablast<CR>
" Open a new empty tab. To open a specific file, type file path after
nnoremap tt :tabedit<Space>
" Go to tab number n
nnoremap tn :tabnext<Space>
" nnoremap td :tabclose<CR>
" not so keen on that one because it seems to close and save without prompting
" Probably something to do with buffers. Should look into it some day
" Move current tab.
nnoremap tm :tabm<Space>
" tm = move it to last position
" tm 0 = move it to first position
" tm {n} = move it to position n
" List all tabs including their displayed windows
nnoremap ts :tabs<CR>

" Shortcut to quickly open and edit this .vimrc file in a new tab
nmap <silent> <leader>ev :tabe $MYVIMRC<cr>
" And to source the .vimrc file so changes take effect
" nmap <silent> <leader>sv :so $MYVIMRC<cr>


" vim-hardtime stuff

" On by default
let g:hardtime_default_on = 1
let g:hardtime_timeout = 1200
"let g:hardtime_showmsg = 1
" Allow a key if it's different from the previous one
let g:hardtime_allow_different_key = 1
" Set maximum number of keypresses (defaults to 1) before it kicks in
let g:hardtime_maxcount = 2

" Recognise Vagrantfiles as Ruby syntax
au BufRead,BufNewFile Vagrantfile set ft=ruby
