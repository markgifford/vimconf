On a new machine, make sure Vim 7.4+ is installed.

mkdir -p ~/.vim/bundle

git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim

vim +PluginInstall +qall to download and install the plugins
